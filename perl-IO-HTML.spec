%define mod_name IO-HTML

Name:           perl-%{mod_name}
Version:        1.004
Release:        3
Summary:        Open an HTML file with automatic charset detection
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/C/CJ/CJM/%{mod_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker) >= 6.30
BuildRequires:  perl(Carp) perl(Encode) >= 2.10 perl(Exporter) >= 5.57

BuildRequires:  perl(File::Temp) perl(Scalar::Util) perl(Test::More) >= 0.88

%description
This module opens a file and performs automatic charset detection
based on the HTML5 algorithm.  You can then pass the filehandle to
HTML::Parser or a related module (or just read it yourself).

%package help
Summary:        Documentation for perl-%{mod_name}

%description help
Documentation for perl-%{mod_name}.

%prep
%autosetup -n %{mod_name}-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{__chmod} -Rf a+rX,u+w,g-w,o-w %buildroot/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sat Jan 18 2025 Funda Wang <fundawang@yeah.net> - 1.004-3
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 hongjinghao <hongjinghao@huawei.com> - 1.004-2
- Add mod_name Macro

* Fri Dec  3 2021 guozhaorui <guozhaorui1@huawei.com> - 1.004-1
- update version to 1.004

* Thu Oct 24 2019 Zaiwang Li <lizaiwang1@huawei.com> - 1.001-13
- Init Package.

